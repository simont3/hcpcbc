Archive Structure
=================

The collected chargeback reports are stored in a way that makes it somewhat easy to
process them later on.

::

    ['3 stores']['2 local'|'3 compliant']['2 path']/
        ['1 targets'][idx]['4 folder']/
            <tenant>/
                __raw/
                    day/
                    hour/
                    total/
                __tenant/
                    day/
                    hour/
                    total/
                <namespace>/
                    day/
                    hour/
                    total/
                <namespace>/
                    day/
                    hour/
                    total/


**Example:**

.. image:: _static/output.png


Output file content overlap
---------------------------

Subsequent reports will likely show some overlap. For example, you might find
the time portion of the last record's *endTime* to be different from
'23:59:59'. The next report will then have a first record where the
*startTime*\ 's time portion is '00:00:00', overlapping with the later one.

**When processing the reports, you need to make sure to check for this overlap
and skip all records not having a full hour (or full day).**
