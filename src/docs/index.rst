HCP Chargeback Collector (|release|)
====================================

Hitachi Content Platform (HCP) allows to generate chargeback reports from
either the HCP System Management Console or through the Management API (MAPI).

A chargeback report contains current and historical storage usage statistics
for HCP tenants and their namespaces. For each chargeback report, you specify
the start and end dates of the report period, which is the time period that’s
covered by the report, and select the reporting interval, which determines
whether HCP generates hourly, daily, or total storage usage statistics for the
specified report period. **Whereas the System Management Console allows for
chargeback reports to be generated for the past 30 days, this tool can go back
up to 180 day, and it stores the reports locally to allow for investigation
over the HCPs lifetime, given that the reports have been collected on a regular
basis.**

Chargeback reports are a good source of information for system analysis,
enabling you to adjust storage and bandwidth allocations based on usage
patterns. These reports can also serve as input to billing systems that need to
determine charges for capacity and bandwidth usage at the tenant or namespace
level.

Chargeback reports cover only HCP tenants and their namespaces. They do not
include the default tenant or its namespace.


..  toctree::
    :maxdepth: 2

    15_installation
    20_usage
    30_config
    40_output
    50_charts
    97_changelog
    98_license
    99_about

..
    Indices and tables
    ==================

    *   :ref:`genindex`
    *   :ref:`modindex`
    *   :ref:`search`

