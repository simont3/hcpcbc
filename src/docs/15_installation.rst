Setup
=====

Pre-requisites
--------------

*   HCP running at least version 5.0 (tested up to version 9.1)

*   A system-level admin account having at least the *Monitor* role

*   MAPI enabled at system-level **and** within each Tenant for which
    chargeback reports are to be collected

*   If the chargeback reports are to be archived to an HCP Namespace: a *data
    access user* having at least write access to that Namespace


Dependencies
------------

You need to have at least Python 3.4 installed to run **hcpcbc**.

It depends on the `hcpsdk <http://hcpsdk.readthedocs.org/en/latest/>`_ to
access HCP and `XlsxWriter <http://xlsxwriter.readthedocs.org>`_ for chart
generation.

.. Tip::

    It's suggested to use a virtual environment to fence the dependency
    from your primary Python environment.


Installation
------------

**Make sure you have Python 3.4 (or better) installed**

In case it's not installed, get it
`here <https://www.python.org/downloads/>`_.

There are two ways to install **hcpcbc**:

1.  system-wide

    *   Install **hcpcbc** by running:

        ..  code-block:: text
            :emphasize-lines: 1

            $ pip install hcpcbc

    -or-

    *   Get the source from
        `gitlab.com <https://gitlab.com/simont3/hcpcbc>`_  either

        *   by downloading the source archive, or

        *   by cloning the repository:

            ..  code-block:: text
                :emphasize-lines: 1

                $ git clone https://gitlab.com/simont3/hcpcbc


    *   Install locally, including the dependency:

        ..  code-block:: text
            :emphasize-lines: 1

            $ python setup.py install


2.   in a virtual environment

    **WINDOWS**

    *   Create a fresh virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            C:\>\Python35\Tools\scripts\pyvenv.py c:\temp\_venv_hcpcbc

    *   Activate the virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1,3

            C:\>cd temp

            C:\temp>\temp\_venv_hcpcbc\Scripts\activate.bat
            (_venv_hcpcbc) C:\temp>

    *   Install **hcpcbc**:

        ..  code-block:: text
            :emphasize-lines: 1

            (_venv_hcpcbc) C:\Temp>pip install hcpcbc
            Collecting hcpcbc
              Downloading hcpcbc-2.1.0.tar.gz
            Collecting hcpsdk>=0.9.4.post0 (from hcpcbc==2.1.0)
              Downloading hcpsdk-0.9.4.post1.tar.gz
            Collecting XlsxWriter>=0.7.7 (from hcpcbc==2.1.0)
              Downloading XlsxWriter-0.7.8-py2.py3-none-any.whl (133kB)
            Collecting dnspython3==1.12.0 (from hcpsdk>=0.9.4.post0->hcpcbc)
              Downloading dnspython3-1.12.0.zip (226kB)
            Installing collected packages: dnspython3, hcpsdk, hcpcbc
              Running setup.py install for dnspython3
              Running setup.py install for hcpsdk
              Running setup.py install for hcpcbc
            Successfully installed XlsxWriter-0.7.8 dnspython3-1.12.0 \
                                   hcpcbc-2.1.0 hcpsdk-0.9.4.post1


    Now you can run **hcpcbc** as long as you have the virtual environment
    activated:

    ..  code-block:: text
        :emphasize-lines: 1

        (_venv_hcpcbc) C:\temp>hcpcbc
        A configuration file is not available.
        Do you want me to create a template for you (y/n)? y
        Creation of template config file "hcpcbc_config.ini" was successfull
            You need to edit it to fit your needs!

    **Linux**

    *   Create a fresh virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            $ pyvenv _venv_hcpcbc

    *   Activate the virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            $ source _venv_hcpcbc/bin/activate


    *   Install **hcpcbc**:

        ..  code-block:: text
            :emphasize-lines: 1

            (_venv_hcpcbc) $ pip install hcpcbc
            Collecting hcpcbc
              Downloading hcpcbc-2.1.0.tar.gz
            Collecting hcpsdk>=0.9.4.post0 (from hcpcbc==2.1.0)
              Downloading hcpsdk-0.9.4.post1.tar.gz
            Collecting XlsxWriter>=0.7.7 (from hcpcbc==2.1.0)
              Downloading XlsxWriter-0.7.8-py2.py3-none-any.whl (133kB)
            Collecting dnspython3==1.12.0 (from hcpsdk>=0.9.4.post0->hcpcbc)
              Downloading dnspython3-1.12.0.zip (226kB)
            Installing collected packages: dnspython3, hcpsdk, hcpcbc
              Running setup.py install for dnspython3
              Running setup.py install for hcpsdk
              Running setup.py install for hcpcbc
            Successfully installed XlsxWriter-0.7.8 dnspython3-1.12.0 \
                                   hcpcbc-2.1.0 hcpsdk-0.9.4.post1



    Now you can run **hcpcbc** as long as you have the virtual environment
    activated:

    ..  code-block:: text
        :emphasize-lines: 1

        (_venv_hcpcbc) $ hcpcbc
        A configuration file is not available.
        Do you want me to create a template for you (y/n)? y
        Creation of template config file "hcpcbc_config.json" was successfull
                You need to edit it to fit your needs!
