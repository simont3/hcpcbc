Usage
=====

**HCP Chargeback Collector** is a command line tool.

::

    $ hcpcbc --help
    usage: hcpcbc [-h] [--version] [-i config.json] [-u] [-c]
                  [--pastdays PASTDAYS]

    hcpcbc downloads the chargeback reports from one or multiple HCP systems, as
    setup in the config file. It keeps a timestamp of the last successfull
    download and excludes everything prior to the last time hcpcbc has been run.

    optional arguments:
      -h, --help           show this help message and exit
      --version            show program's version number and exit
      -i config.json       path/name of the file containing the configuration
                           (defaults to the current directory)
      -u                   update the Tenant list(s) and stop afterwards
      -c                   create charts from existing reports, only (do nothing
                           else)
      --pastdays PASTDAYS  start collection x days in the past


Arguments:
----------

    ``-i configfile.json``
        is relevant if your config file is *not*
        **hcpcbc_config.json** in the current directory.

    ``-u``
        allows to update the Tenant list(s) only, without collecting any
        reports.

    ``-c``
        create charts from the already available reports, not updating the
        Tenant list(s) or collecting new reports.

    ``--pastdays PASTDAYS``
        start collection PASTDAYS in the past - once for each Tenant that was
        not collected before.


..  Tip::

    If you don't have a configuration file yet, just run the tool without
    arguments; it will create a template config file for you::

        $ hcpcbc
        A configuration file is not available.
        Do you want me to create a template for you (y/n)? y
        Creation of template config file "hcpcbc_config.json" was successfull
                You need to edit it to fit your needs!

    | **Make sure you edit the file to fit your needs!**
    | (see :doc:`/30_config` for details)


**hcpcbc** is made to run regularly. You might want to run it once a week or
so, to build up a pool of report data for later investigation.


**A run's output:**

..  code-block:: text
    :linenos:

    01/06 21:32:06 [INFO    ] started run (user "sm")
    01/06 21:32:06 [INFO    ] updating the Tenant list for "hcp72.archivas.com"
    01/06 21:32:08 [INFO    ] updating the Tenant list for "hcp73.archivas.com"
    01/06 21:32:08 [WARNING ] 	Default Tenant found - no chargeback support
    01/06 21:32:10 [INFO    ] working on target "hcp72.archivas.com" (idx=0)
    01/06 21:32:10 [INFO    ] 	collecting from Tenant "m"
    01/06 21:32:31 [INFO    ] 	tranfering reports to local store
    01/06 21:32:31 [INFO    ] 	collecting from Tenant "s3"
    01/06 21:32:37 [INFO    ] 	tranfering reports to local store
    01/06 21:32:37 [INFO    ] 	collecting from Tenant "s3erlei"
    01/06 21:32:49 [INFO    ] 	tranfering reports to local store
    01/06 21:32:49 [INFO    ] 	collecting from Tenant "swifttest"
    01/06 21:32:50 [INFO    ] 	tranfering reports to local store
    01/06 21:32:50 [INFO    ] 	collecting from Tenant "testtenant"
    01/06 21:32:50 [INFO    ] 	--> unable to collect namespace details
    01/06 21:32:53 [INFO    ] 	tranfering reports to local store
    01/06 21:32:53 [INFO    ] 	collecting from Tenant "swifty"
    01/06 21:32:59 [INFO    ] 	tranfering reports to local store
    01/06 21:32:59 [INFO    ] 	collecting from Tenant "x"
    01/06 21:32:59 [INFO    ] 	--> unable to collect namespace details
    01/06 21:32:59 [INFO    ] 	tranfering reports to local store
    01/06 21:32:59 [INFO    ] working on target "hcp73.archivas.com" (idx=1)
    01/06 21:32:59 [INFO    ] 	collecting from Tenant "aw21"
    01/06 21:33:03 [INFO    ] 	tranfering reports to local store
    01/06 21:33:03 [INFO    ] 	collecting from Tenant "m"
    01/06 21:33:11 [INFO    ] 	tranfering reports to local store
    01/06 21:33:11 [INFO    ] 	collecting from Tenant "s3"
    01/06 21:33:16 [INFO    ] 	tranfering reports to local store
    01/06 21:33:16 [INFO    ] 	collecting from Tenant "swifttest"
    01/06 21:33:32 [INFO    ] 	tranfering reports to local store
    01/06 21:33:32 [INFO    ] 	collecting from Tenant "s3erlei"
    01/06 21:33:39 [INFO    ] 	tranfering reports to local store
    01/06 21:33:39 [INFO    ] creating charts for HCP "hcp72.archivas.com"
    01/06 21:33:39 [INFO    ] 	working on Tenant "m"
    01/06 21:33:56 [INFO    ] 	working on Tenant "s3"
    01/06 21:34:02 [INFO    ] 	working on Tenant "s3erlei"
    01/06 21:34:16 [INFO    ] 	working on Tenant "swifttest"
    01/06 21:34:18 [INFO    ] 	working on Tenant "testtenant"
    01/06 21:34:18 [INFO    ] 	working on Tenant "swifty"
    01/06 21:34:28 [INFO    ] 	working on Tenant "x"
    01/06 21:34:29 [INFO    ] 	working on Tenant comparison charts
    01/06 21:34:52 [INFO    ] creating charts for HCP "hcp73.archivas.com"
    01/06 21:34:52 [INFO    ] 	working on Tenant "aw21"
    01/06 21:34:56 [INFO    ] 	working on Tenant "m"
    01/06 21:35:08 [INFO    ] 	working on Tenant "s3"
    01/06 21:35:15 [INFO    ] 	working on Tenant "swifttest"
    01/06 21:35:43 [INFO    ] 	working on Tenant "s3erlei"
    01/06 21:35:54 [INFO    ] 	working on Tenant comparison charts
    01/06 21:36:04 [INFO    ] finished run (user "sm")


A few things to notice:

lines 2-3:

    as a first action, a list of existing Tenants is acquired
    from each configured HCP system

line 4:

    the Default Tenant doesn't provide chargeback reports

starting at line 5,

    collection for the first configured HCP system begins, Tenant by Tenant.

line 15,20:

    if the HCP user account used by **hcpcbc** doesn't have access to the
    Tenant, Tenant summaries are collectable, only
