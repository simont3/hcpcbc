# this is a simple analysis of a single ChargeBack csv file gained from the HCP GUI

import sys
from csv import DictReader

def calcbytesize(nbytes, kb=False, mb=False, formLang=False, nozero=False):
    '''
    Return a given no. of Bytes in a human readable format.
    '''
    if nozero and not nbytes:
        return('')

    sz = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
    szl = ["Byte", "Kibibyte", "Mebibyte", "Gibibyte", "Tebibyte",
           "Pebibyte", "Exbibyte", "Zebibyte", "Yobibyte"]
    i = 0
    neg = False
    if kb:
        nbytes *= 1024  # we got kibibytes!
    if mb:
        nbytes *= 1024*1024  # we got mibibytes!

    if nbytes < 0:
        neg = True
        nbytes *= -1

    while nbytes > 1023:
                    nbytes = nbytes / 1024
                    i = i + 1
    if neg:
        nbytes *= -1

    # if round(nbytes) < nbytes:
    #     nbytes = round(nbytes)+1
    # else:
    #     nbytes = round(nbytes)

    if formLang:
        if szl[i] == 'Byte':
            return("{0:.0f} {1:}".format(nbytes, szl[i]))
        else:
            return ("{0:.2f} {1:}".format(nbytes, szl[i]))
    else:
        if sz[i] == 'B':
            return("{0:.0f} {1:}".format(nbytes, sz[i]))
        else:
            return("{0:.2f} {1:}".format(nbytes, sz[i]))


if len(sys.argv) != 2:
    sys.exit('fatal: file read is missing')

with open(sys.argv[1], 'r') as ihdl:
    reader = DictReader(ihdl)

    tn = ns = ''
    d = {'namespaceName': '',
         'startTime': '',
         'endTime': '',
         'objectCount': 0,
         'ingestedVolume': 0,
         'storageCapacityUsed': 0,
         'bytesIn': 0,
         'bytesOut': 0,
         'reads': 0,
         'writes': 0,
         'deletes': 0,
         'numrecs': 0}

    for row in reader:
        if row['tenantName'] != tn:
            tn = row['tenantName']

        if row['namespaceName'] == ns:
                d["namespaceName"] = row["namespaceName"]
                d["endTime"] = row["endTime"]
                d['objectCount'] = row['objectCount']
                d['ingestedVolume'] = row["ingestedVolume"]
                d['storageCapacityUsed'] = row["storageCapacityUsed"]
                d['bytesIn'] += int(row["bytesIn"])
                d['bytesOut'] += int(row["bytesOut"])
                d['reads'] += int(row["reads"])
                d['writes'] += int(row["writes"])
                d['deletes'] += int(row["deletes"])
                d['numrecs'] += 1
        else:
            # Namespace close code goes here
            if d['numrecs']:
                print(f'\tNamespace: {d["namespaceName"]}')
                print(f'\t\t          startTime: {d["startTime"]}')
                print(f'\t\t            endTime: {d["endTime"]}')
                print(f'\t\t        objectCount: {int(row["objectCount"]):>16,d}')
                print(f'\t\t     ingestedVolume: {calcbytesize(int(row["ingestedVolume"])):>22}')
                print(f'\t\tstorageCapacityUsed: {calcbytesize(int(row["storageCapacityUsed"])):>22}')
                print('\t\t------------averages:')
                print(f'\t\t          bytesIn/h: {calcbytesize(int(d["bytesIn"]) / d["numrecs"]):>22}')
                print(f'\t\t         bytesOut/h: {calcbytesize(int(d["bytesOut"]) / d["numrecs"]):>22}')
                print(f'\t\t            reads/h: {int(d["reads"]) / d["numrecs"]:>19,.2f}')
                print(f'\t\t           writes/h: {int(d["writes"]) / d["numrecs"]:>19,.2f}')
                print(f'\t\t          deletes/h: {int(d["deletes"]) / d["numrecs"]:>19,.2f}')
                print()
            ns = row['namespaceName']

            d["namespaceName"] = row["namespaceName"]
            d["startTime"] = row["startTime"]
            d["endTime"] = row["endTime"]
            d['objectCount'] = row['objectCount']
            d['ingestedVolume'] = row["ingestedVolume"]
            d['storageCapacityUsed'] = row["storageCapacityUsed"]
            d['bytesIn'] = int(row["bytesIn"])
            d['bytesOut'] = int(row["bytesOut"])
            d['reads'] = int(row["reads"])
            d['writes'] = int(row["writes"])
            d['deletes'] = int(row["deletes"])
            d['numrecs'] = 1

        # collect Tenant data
        if not ns:
            d["namespaceName"] = row["namespaceName"]
            d["startTime"] = row["startTime"]
            d["endTime"] = row["endTime"]
            d['objectCount'] = row['objectCount']
            d['ingestedVolume'] = row["ingestedVolume"]
            d['storageCapacityUsed'] = row["storageCapacityUsed"]
            d['bytesIn'] += int(row["bytesIn"])
            d['bytesOut'] += int(row["bytesOut"])
            d['reads'] += int(row["reads"])
            d['writes'] += int(row["writes"])
            d['deletes'] += int(row["deletes"])
            d['numrecs'] += 1

    # Tenant close code goes here
    print()
    print(f'Tenant: {row["tenantName"]}')
    print(f'\t\t          startTime: {row["startTime"]}')
    print(f'\t\t            endTime: {row["endTime"]}')
    print(f'\t\t        objectCount: {int(row["objectCount"]):>16,d}')
    print(f'\t\t     ingestedVolume: {calcbytesize(int(row["ingestedVolume"])):>22}')
    print(f'\t\tstorageCapacityUsed: {calcbytesize(int(row["storageCapacityUsed"])):>22}')
    print('\t\t------------averages:')
    print(f'\t\t          bytesIn/h: {calcbytesize(int(d["bytesIn"]) / d["numrecs"]):>22}')
    print(f'\t\t         bytesOut/h: {calcbytesize(int(d["bytesOut"]) / d["numrecs"]):>22}')
    print(f'\t\t            reads/h: {int(d["reads"]) / d["numrecs"]:>19,.2f}')
    print(f'\t\t           writes/h: {int(d["writes"]) / d["numrecs"]:>19,.2f}')
    print(f'\t\t          deletes/h: {int(d["deletes"]) / d["numrecs"]:>19,.2f}')
